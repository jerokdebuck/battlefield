#include "Functions.h"
#include "InfantryPrimary.h"
#include "EngineerPrimary.h"
#include "SupportPrimary.h"
#include "SniperPrimary.h"
#include "Secondary.h"
#include <string>
class Weapons;

using std::string;


Weapon * weaponReturn(string weaponName)
{
 
  string weaponList[20] = {"MP433",".44 Magnum","M1911","Glock 17","AK-47","M16","FN2000","AEK-971","AKS-74u","M4","SCAR-H","G36C","M27","RPK","M249","PKP Pecheneg","MK11","SVD","SV98","M82"};
  int find;
  int i;
  
  for (i = 0; i < 20; i++)
  {
    if (weaponName.compare(weaponList[i]) == 0)
    {
      find = i;
      break;
    }
  }

  switch (find)
  {
  case 0:
    return new MP433();
  case 1:
    return new Magnum();
  case 2: 
    return new M1911();
  case 3:
    return new Glock();
  case 4:
    return new AK47();
  case 5:
    return new M16();
  case 6:
    return new FN2000();
  case 7:
    return new AEK971();
  case 8: 
    return new AKS();
  case 9:
    return new M4();
  case 10:
    return new SCAR();
  case 11:
    return new G36C();
  case 12:
    return new M27();
  case 13:
    return new RPK();
  case 14:
    return new M249();
  case 15:
    return new PKP();
  case 16:
    return new MK11();
  case 17:
    return new SVD();
  case 18:
    return new SV98();
  case 19:
    return new M82();
  }
  return new AK47();
}


