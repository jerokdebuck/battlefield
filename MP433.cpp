#include "Secondary.h"
#include "Soldier.h"
#include <string>

using std::string;

MP433::MP433()
{
  name = "MP433";
}

const MP433 & MP433::operator=(const MP433 & fromMP)
{
  if (this != &fromMP)
  {
    name = fromMP.name;
    damage = fromMP.damage;
    accuracy = fromMP.accuracy;
    magSize = fromMP.magSize;
  }
  return *this;
}

string MP433::getName(void)
{
        return name;
}

void MP433::inputDamage(void)
{
        damage = 20;
}

int MP433::getDamage(void)
{
        return damage;
}

void MP433::inputAccuracy(void)
{
	accuracy = 23;
}

int MP433::getAccuracy(void)
{
        return accuracy;
}

void MP433::inputMagazine(void)
{
        magSize = 18;
}

int MP433::getMagazine(void)
{
        return magSize;
}

bool MP433::isEmpty(void)
{
        if (magSize == 0)
                return true;
        return false;
}

