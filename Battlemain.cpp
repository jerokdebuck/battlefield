#include "Soldier.h"
#include "Weapon.h"
#include "InfantryPrimary.h"
#include "EngineerPrimary.h"
#include "SupportPrimary.h"
#include "SniperPrimary.h"
#include "Secondary.h"
#include "Team.h"
#include <string>
#include <iostream>
#include <vector>
#include <cstdio>
#include <cstring>

using std::endl; 
using std::cout;
using std::cin;
using std::string;


void insertWeapon( Weapon & weapon)
{
        string weaponName = weapon.getName();
	      weapon.insertName(weaponName);
        weapon.inputDamage();
        weapon.inputAccuracy();
        weapon.inputMagazine();
}


int main(void)
{
	//Create a team
  cout << "How many for American team? : " << endl;
  int teamNum;
  cin >> teamNum;
  if(teamNum == 0)
    teamNum = 1;

	std::vector<Soldier *> Team1;
  int sold;
  for(sold = 0; sold < teamNum ; sold++)
  {
    cout << "Enter name: " << endl;
    string soldName;
    cin >> soldName;

    cout << "Enter rank: " << endl;
    int rank;
    cin >> rank;

    cout << "Enter side: " << endl;
    string side;
    cin >> side; //American or Russian

    cout << "Enter type: " << endl; 
    /* Infantry,Engineer, Support, Sniper */
    string type;
    cin >> type;

    if(type.compare("Infantry") == 0)
	    Team1.push_back(new Infantry(100,100,rank,100*rank,soldName,side));
    
    else if (type.compare("Engineer") == 0)
      Team1.push_back(new Engineer(100,100,rank,100*rank,soldName,side));
    
    else if (type.compare("Support") == 0)
      Team1.push_back(new Support(100,100,rank,100*rank,soldName,side));
    
    else if (type.compare("Sniper") == 0)
      Team1.push_back(new Sniper(100,100,rank,100*rank,soldName,side));
    Team1[sold] -> outputSoldiers();
  }
  
 
	Team * Americans = new American(Team1,teamNum);

	int size = Team1.size(); 
	unsigned int i;
	for(i = 0; i < size; i++)
  {
    Weapon * primary = NULL;
    Weapon * secondary = NULL;
    primary = Team1[i] -> choosePrimary();
    secondary = Team1[i] -> chooseSecondary();
    insertWeapon(*primary);
    insertWeapon(*secondary); 
    Team1[i] -> lockPrimary(primary);
    Team1[i] -> lockSecondary(secondary);
    Team1[i] -> outputPrimary();
    Team1[i] -> outputSecondary();

  }
  Team1[0] -> getDamage(150);
  Team1[1] -> getDamage(99);
      
	std::vector<Soldier *> liveAmericans = Americans -> aliveMembers();
  cout << "ALIVE MEMBERS OF AMERICAN TEAM" << endl;  
  for(i = 0; i < liveAmericans.size(); i++)
  {
    liveAmericans[i] -> outputSoldiers();
    liveAmericans[i] -> outputPrimary();
    liveAmericans[i] -> outputSecondary();
  }
  
  std::vector<Soldier *> deadAmericans = Americans -> deadMembers();
  cout << "DEAD MEMBERS OF AMERICAN TEAM" << endl;
  for(i = 0; i < deadAmericans.size(); i++)
  {
    deadAmericans[i] -> outputSoldiers();
    deadAmericans[i] -> outputPrimary();
    deadAmericans[i] -> outputSecondary();
  }
  

  for(std::vector<Soldier *>::iterator it = Team1.begin(); it != Team1.end(); ++it)
    delete *it;

 
  delete Americans; 

	return 0;
}
