#include <string>
#include "Functions.h"

class Soldier;

#ifndef WEAPON_H
#define WEAPON_H

using std::string;

class Weapon
{
        protected:
                string name;
                int damage;
                int accuracy;
                int magSize;
        public:
                void insertName(string);

                Weapon();
		            virtual ~Weapon();
		
                virtual string getName(void) = 0;
                virtual void inputDamage(void) = 0;
                virtual int getDamage(void) = 0;
                virtual void inputAccuracy(void) = 0;
                virtual int getAccuracy(void) = 0;
                virtual void inputMagazine(void) = 0;
                virtual int getMagazine(void) = 0;
                virtual bool isEmpty(void) = 0;
        
                const Weapon & operator=(const Weapon &);
};

#endif
