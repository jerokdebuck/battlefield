#include <string>
#include "Soldier.h"
#include "Weapon.h"

#ifndef ENGINEERPRIMARY_H
#define ENGINEERPRIMARY_H

using std::string;

/* 
class infantryWeapon: public Weapon
{
        protected:
                string name;
                int damage;
                int accuracy;
                int magSize;
        public:
                void insertName(string wepName)
                { name = wepName; }
                virtual string getName(void) = 0;
                virtual void inputDamage(void) = 0;
                virtual int getDamage(void) = 0;
                virtual void inputAccuracy(void) = 0;
                virtual int getAccuracy(void) = 0;
                virtual void inputMagazine(void) = 0;
                virtual int getMagazine(void) = 0;
                virtual bool isEmpty(void) = 0;
};
*/

class AKS: public Weapon
{
        public:
                AKS();
                string getName(void);
                void inputDamage(void);
                int getDamage(void);
                void inputAccuracy(void);
                int getAccuracy(void);
                void inputMagazine(void);
                int getMagazine(void);
                bool isEmpty(void);
};

class M4: public Weapon
{
        public:
                M4();
                string getName(void);
                void inputDamage(void);
                int getDamage(void);
                void inputAccuracy(void);
                int getAccuracy(void);
                void inputMagazine(void);
                int getMagazine(void);
                bool isEmpty(void);
};

class SCAR: public Weapon
{
        public:
                SCAR();
                string getName(void);
                void inputDamage(void);
                int getDamage(void);
                void inputAccuracy(void);
                int getAccuracy(void);
                void inputMagazine(void);
                int getMagazine(void);
                bool isEmpty(void);
};

class G36C: public Weapon
{
        public:
                G36C();
                string getName(void);
                void inputDamage(void);
                int getDamage(void);
                void inputAccuracy(void);
                int getAccuracy(void);
                void inputMagazine(void);
                int getMagazine(void);
                bool isEmpty(void);
};

#endif
