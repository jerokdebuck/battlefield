#include "Secondary.h"
#include "Soldier.h"
#include <string>

using std::string;

Magnum::Magnum()
{
  name = ".44 Magnum";
}

string Magnum::getName(void)
{
        return name;
}

void Magnum::inputDamage(void)
{
        damage = 50;
}

int Magnum::getDamage(void)
{
        return damage;
}

void Magnum::inputAccuracy(void)
{
        accuracy = 20;
}

int Magnum::getAccuracy(void)
{
        return accuracy;
}

void Magnum::inputMagazine(void)
{
        magSize = 6;
}

int Magnum::getMagazine(void)
{
        return magSize;
}

bool Magnum::isEmpty(void)
{
        if (magSize == 0)
                return true;
        return false;
}


