#include <string>

#ifndef RIGHTTOOLS_H
#define RIGHTTOOLS_H

using std::string; 

class rightTools
{
  protected:
    string name;
    bool doesDamage;
    int damage;
  
  public:
    rightTools();
    virtual ~rightTools();
  
    virtual string getName(void) = 0;
    virtual void doesDam(void) = 0;
    virtual int getDamage(void) = 0;

}

#endif //RIGHTTOOLS_H
