#include "SniperPrimary.h"
#include "Soldier.h"
#include <string>

using std::string;

MK11::MK11()
{
  name = "MK11";
}

string MK11::getName(void)
{
        return name;
}

void MK11::inputDamage(void)
{
        damage = 40;
}

int MK11::getDamage(void)
{
        return damage;
}

void MK11::inputAccuracy(void)
{
        accuracy = 45;
}

int MK11::getAccuracy(void)
{
        return accuracy;
}

void MK11::inputMagazine(void)
{
        magSize = 10;
}

int MK11::getMagazine(void)
{
	return magSize;
}

bool MK11::isEmpty(void)
{
        if (magSize == 0)
                return true;
        return false;
}


