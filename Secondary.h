#include <string>
#include "Soldier.h"
#include "Weapon.h"
#ifndef SECONDARY_H
#define SECONDARY_H

using std::string;

/*
class SecondaryWeapon: public Weapon
{

        protected:
                string name;
                int damage;
                int accuracy;
                int magSize;
        public:
                void insertName(string wepName)
                { name = wepName; }

                virtual string getName(void) = 0;
                virtual void inputDamage(void) = 0;
                virtual int getDamage(void) = 0;
                virtual void inputAccuracy(void) = 0;
                virtual int getAccuracy(void) = 0;
                virtual void inputMagazine(void) = 0;
                virtual int getMagazine(void) = 0;
                virtual bool isEmpty(void) = 0; 

}; 
*/
class MP433: public Weapon
{
  public:
        MP433();
        const MP433  & operator=(const MP433 &);
        
        string getName(void);
        void inputDamage(void);
        int getDamage(void);
        void inputAccuracy(void);
        int getAccuracy(void);
        void inputMagazine(void);
        int getMagazine(void);
        bool isEmpty(void);
};


class Magnum: public Weapon
{
   public:
        Magnum();
        string getName(void);
        void inputDamage(void);
        int getDamage(void);
        void inputAccuracy(void);
        int getAccuracy(void);
        void inputMagazine(void);
        int getMagazine(void);
        bool isEmpty(void);
};


class M1911: public Weapon
{
      public:
        M1911();
        string getName(void);
        void inputDamage(void);
        int getDamage(void);
        void inputAccuracy(void);
        int getAccuracy(void);
        void inputMagazine(void);
        int getMagazine(void);
        bool isEmpty(void);
};


class Glock: public Weapon
{
      public:
        Glock();
	      string getName(void);
        void inputDamage(void);
        int getDamage(void);
        void inputAccuracy(void);
        int getAccuracy(void);
        void inputMagazine(void);
        int getMagazine(void);
        bool isEmpty(void);
};

#endif //SECONDARY_H
