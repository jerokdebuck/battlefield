#include "EngineerPrimary.h"
#include "Soldier.h"
#include <string>

using std::string;

G36C::G36C()
{
  name = "G36C";
}

string G36C::getName(void)
{
        return name;
}

void G36C::inputDamage(void)
{
        damage = 33;
}       

int G36C::getDamage(void)
{
        return damage;
}       

void G36C::inputAccuracy(void)
{
        accuracy = 30;
}       

int G36C::getAccuracy(void)
{
        return accuracy;
}       

void G36C::inputMagazine(void)
{
        magSize = 30;
}       


int G36C::getMagazine(void)
{
        return magSize;
}

bool G36C::isEmpty(void)
{
        if (magSize == 0)
                return true;
        return false;
}

          
