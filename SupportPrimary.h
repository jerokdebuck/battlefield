#include <string>
#include "Soldier.h"
#include "Weapon.h"

#ifndef SUPPORTPRIMARY_H
#define SUPPORTPRIMARY_H

using std::string;

/* 
class supportWeapon: public Weapon
{
        protected:
                string name;
                int damage;
                int accuracy;
                int magSize;
        public:
                void insertName(string wepName)
                { name = wepName; }
                virtual string getName(void) = 0;
                virtual void inputDamage(void) = 0;
                virtual int getDamage(void) = 0;
                virtual void inputAccuracy(void) = 0;
                virtual int getAccuracy(void) = 0;
                virtual void inputMagazine(void) = 0;
                virtual int getMagazine(void) = 0;
                virtual bool isEmpty(void) = 0;
};
*/

class M27: public Weapon
{
        public:
                M27();
                string getName(void);
                void inputDamage(void);
                int getDamage(void);
                void inputAccuracy(void);
                int getAccuracy(void);
                void inputMagazine(void);
                int getMagazine(void);
                bool isEmpty(void);
};

class RPK: public Weapon
{
        public:
                RPK();
                string getName(void);
                void inputDamage(void);
                int getDamage(void);
                void inputAccuracy(void);
                int getAccuracy(void);
                void inputMagazine(void);
                int getMagazine(void);
                bool isEmpty(void);
};

class M249: public Weapon
{
        public:
                M249();
                string getName(void);
                void inputDamage(void);
                int getDamage(void);
                void inputAccuracy(void);
                int getAccuracy(void);
                void inputMagazine(void);
                int getMagazine(void);
                bool isEmpty(void);
};

class PKP: public Weapon
{
        public:
                PKP();
                string getName(void);
                void inputDamage(void);
                int getDamage(void);
                void inputAccuracy(void);
                int getAccuracy(void);
                void inputMagazine(void);
                int getMagazine(void);
                bool isEmpty(void);
};


#endif

