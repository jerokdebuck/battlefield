#include "InfantryPrimary.h"
#include <string>

using std::string;

FN2000::FN2000()
{
  name = "FN2000";
}

string FN2000::getName(void)
{
	return name;
}

void FN2000::inputDamage(void)
{
        damage = 25;
}

int FN2000::getDamage(void)
{
        return damage;
}

void FN2000::inputAccuracy(void)
{
        accuracy = 25;
}

int FN2000::getAccuracy(void)
{
        return accuracy;
}

void FN2000::inputMagazine(void)
{
        magSize = 30;
}

int FN2000::getMagazine(void)
{
        return magSize;
}

bool FN2000::isEmpty(void)
{
	if (magSize == 0)
		return true;
	return false;
}
