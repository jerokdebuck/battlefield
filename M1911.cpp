#include "Secondary.h"
#include "Soldier.h"
#include <string>

using std::string;

M1911::M1911()
{
  name = "M1911";
}

string M1911::getName(void)
{
        return name;
}

void M1911::inputDamage(void)
{
        damage = 34;
}

int M1911::getDamage(void)
{
        return damage;
}

void M1911::inputAccuracy(void)
{
        accuracy = 22;
}

int M1911::getAccuracy(void)
{
        return accuracy;
}

void M1911::inputMagazine(void)
{
        magSize = 9;
}

int M1911::getMagazine(void)
{
        return magSize;
}

bool M1911::isEmpty(void)
{
        if (magSize == 0)
                return true;
        return false;
}


