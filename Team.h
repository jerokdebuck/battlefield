class Soldier;
class Weapon;
#include <string>
#include <vector>

#ifndef TEAM_H
#define TEAM_H

class Team
{
	protected:
		int maxNumber;
    std::vector<Soldier *> soldierList;
    std::vector<Soldier *> liveSoldiers;
    std::vector<Soldier *> deadSoldiers;
	public:
    Team();
    Team(std::vector<Soldier *>,int);

		virtual ~Team();		
    virtual std::vector<Soldier *> getSoldiers() = 0;
    virtual std::vector<Soldier *> aliveMembers() = 0;
    virtual std::vector<Soldier *> deadMembers() = 0;
		virtual bool isOperational() = 0;
		
};

class American: public Team
{
	public:
		American();
		American(std::vector<Soldier *>,int);
    ~American();
		
		bool isOperational(void);
    std::vector<Soldier *> getSoldiers(void);
    std::vector<Soldier *> aliveMembers(void);
    std::vector<Soldier *> deadMembers(void);

};

class Russian: public Team
{
	public:
		Russian();
		Russian(std::vector<Soldier *>,int);
    ~Russian();
		
		bool isOperational(void);
    std::vector<Soldier *> getSoldiers(void);
    std::vector<Soldier *> aliveMembers(void);
    std::vector<Soldier *> deadMembers(void);

};
		
	
		
	
#endif		
