#include "InfantryTools.h"
#include <string>

using std::string;

medicKit::medicKit()
{
  this -> name = "Medic Kit";
}

string medicKit::getName(void)
{
  return name;
}

void medicKit::doesDam(void)
{
  this -> doesDamage = false;
  this -> damage = 0;
}

int medicKit::getDamage(void)
{
  return damage;
}

Defib::Defib()
{
  this -> name = "Defibulator";
}

string Defib::getName(void)
{
  return name;
}

void Defib::doesDam(void)
{
  this -> doesDamage = false;
  this -> damage = 0;
}

int Defib::getDamage(void)
{
  return damage;
}

M320::M320()
{
  this -> name = "M320";
}

string M320::getName(void)
{
  return name;
}

void M320::doesDam(void)
{
  this -> doesDamage = true;
  this -> damage = 60;
}

int getDamage(void)
{
  return damage; 
}
