#include "Weapon.h"

void Weapon::insertName(string name)
{
  this -> name = name;
}

Weapon::Weapon()
{
  name = "AK-47";
}

Weapon::~Weapon()
{
}

const Weapon & Weapon::operator=(const Weapon & fromWeapon)
{
  if (this != & fromWeapon)
  {
    name = fromWeapon.name;
    damage = fromWeapon.damage;
    accuracy = fromWeapon.accuracy;
    magSize = fromWeapon.magSize;
  }
  return *this;
}
