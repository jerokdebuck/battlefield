#include <string>
#include "Soldier.h"
#include "Weapon.h"

#ifndef SNIPERPRIMARY_H
#define SNIPERPRIMARY_H

using std::string;

/* 
class sniperWeapon: public Weapon
{
        protected:
                string name;
                int damage;
                int accuracy;
                int magSize;
        public:
                void insertName(string wepName)
                { name = wepName; }
                virtual string getName(void) = 0;
                virtual void inputDamage(void) = 0;
                virtual int getDamage(void) = 0;
                virtual void inputAccuracy(void) = 0;
                virtual int getAccuracy(void) = 0;
                virtual void inputMagazine(void) = 0;
                virtual int getMagazine(void) = 0;
                virtual bool isEmpty(void) = 0;
};
*/

class MK11: public Weapon
{
        public:
                MK11();
                string getName(void);
                void inputDamage(void);
                int getDamage(void);
                void inputAccuracy(void);
                int getAccuracy(void);
                void inputMagazine(void);
                int getMagazine(void);
                bool isEmpty(void);
};

class SVD: public Weapon
{
        public:
                SVD();
                string getName(void);
                void inputDamage(void);
                int getDamage(void);
                void inputAccuracy(void);
                int getAccuracy(void);
                void inputMagazine(void);
                int getMagazine(void);
                bool isEmpty(void);
};

class SV98: public Weapon
{
        public:
                SV98();
                string getName(void);
                void inputDamage(void);
                int getDamage(void);
                void inputAccuracy(void);
                int getAccuracy(void);
                void inputMagazine(void);
                int getMagazine(void);
                bool isEmpty(void);
};

class M82: public Weapon
{
        public:
                M82();
                string getName(void);
                void inputDamage(void);
                int getDamage(void);
                void inputAccuracy(void);
                int getAccuracy(void);
                void inputMagazine(void);
                int getMagazine(void);
                bool isEmpty(void);
};

#endif



