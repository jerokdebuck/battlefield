#include "SupportPrimary.h"
#include "Soldier.h"
#include <string>

using std::string;

RPK::RPK()
{
  name = "RPK";
}

string RPK::getName(void)
{
        return name;
}

void RPK::inputDamage(void)
{
        damage = 50;
}

int RPK::getDamage(void)
{
        return damage;
}

void RPK::inputAccuracy(void)
{
        accuracy = 18;
}

int RPK::getAccuracy(void)
{
        return accuracy;
}

void RPK::inputMagazine(void)
{
        magSize = 30;
}

int RPK::getMagazine(void)
{
	return magSize;
}

bool RPK::isEmpty(void)
{
        if (magSize == 0)
                return true;
        return false;
}


