#include <string>
#include "rightTools.h"

#ifndef INFANTRYTOOLS_H
#define INFANTRYTOOLS_H

class medicKit
{
  medicKit();
  
  string getName(void);
  void doesDam(void);
  int getDamage(void);

}


class Defib
{
  Defib();
  
  string getName(void);
  void doesDam(void);
  int getDamage(void);
}


class M320
{
  M320();
  
  string getName(void);
  void doesDam(void);
  int getDamage(void);
}



#endif //INFANTRYTOOLS_H
