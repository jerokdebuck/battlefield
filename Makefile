
CC = g++
CFLAGS = -std=c++98 -c -O -ansi -Wall -Wextra 

battle: Functions.o Battlemain.o Soldier.o Infantry.o Engineer.o Support.o Sniper.o Weapon.o AK-47.o M16.o FN2000.o AEK-971.o AKS.o M4.o Scar.o G36C.o M27.o RPK.o M249.o PKP.o MK11.o SVD.o SV98.o M82.o MP433.o M1911.o Glock.o 44Magnum.o Team.o America.o Russian.o
	$(CC)	-o battle $^
clean:
	rm -f *.o 
%.o: %.cpp
	$(CC) $(CFLAGS) $^

