#include "InfantryPrimary.h"
#include <string>

using std::string;

AEK971::AEK971()
{
  name = "AEK-971";
}

string AEK971::getName(void)
{
	return name;
}

void AEK971::inputDamage(void)
{
        damage = 40;
}

int AEK971::getDamage(void)
{
        return damage;
}

void AEK971::inputAccuracy(void)
{
        accuracy = 12;
}

int AEK971::getAccuracy(void)
{
        return accuracy;
}

void AEK971::inputMagazine(void)
{
        magSize = 30;
}

int AEK971::getMagazine(void)
{
        return magSize;
}

bool AEK971::isEmpty(void)
{
	if (magSize == 0)
		return true;
	return false;
}
