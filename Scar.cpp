#include "EngineerPrimary.h"
#include "Soldier.h"
#include <string>

using std::string;

SCAR::SCAR()
{
  name = "SCAR-H";
}

string SCAR::getName(void)
{
        return name;
}

void SCAR::inputDamage(void)
{
        damage = 35;
}

int SCAR::getDamage(void)
{
        return damage;
}

void SCAR::inputAccuracy(void)
{
        accuracy = 35;
}

int SCAR::getAccuracy(void)
{
        return accuracy;
}

void SCAR::inputMagazine(void)
{
        magSize = 20;
}

int SCAR::getMagazine(void)
{
        return magSize;
}

bool SCAR::isEmpty(void)
{
        if (magSize == 0)
                return true;
        return false;
}

