#include "Soldier.h"
#include "Weapon.h"
#include "InfantryPrimary.h"
#include "EngineerPrimary.h"
#include "SupportPrimary.h"
#include "SniperPrimary.h"
#include "Secondary.h"
#include "Functions.h"
#include <string>
#include <vector>
#include <iostream>
#include <cstdlib>

using std::string;
using std::cin;
using std::cout;
using std::endl;


Support::Support()
{
  this -> name = "HEAVY";
	this -> teamName = "American";
  this -> xp = 0;
  this -> rank = 1;
  this -> health = 100;
  this -> stamina = 100;
}

Support::Support(int health, int stamina, int rank, int xp, string name, string teamName)
{
 if (health > 100)
    health = 100;
  else if (health < 0)
    health = 0;

  if (stamina < 0)
    stamina = 0;
  else if (stamina > 100)
    stamina = 100;
  
  if (rank <= 0)
    rank = 1;
  else if (rank > 45)
    rank = 45;
  
  if((teamName.compare("American") != 0) && teamName.compare("Russian") != 0)
  {
    int x = rand() % 2;
    if (x == 0)
      teamName = "American";
    else if (x == 1)
      teamName = "Russian";
  }

  this -> health = health;
  this -> stamina = stamina;
  this -> rank = rank;
  this -> xp = xp;
  this -> name = name;
	this -> teamName = teamName;
}

Support::~Support()
{
 // delete primary;
//  delete secondary;
}

const Support & Support::operator=(const Support & fromSupport)
{
	if (this != & fromSupport)
	{
		health = fromSupport.health;
		stamina = fromSupport.stamina;
		rank = fromSupport.rank;
		name = fromSupport.name;
		teamName = fromSupport.teamName;
	}
	return *this;
}

string Support::getType(void)
{
        return "Support";
}

string Support::getTeamName(void)
{
	return teamName;
}

string Support::getName(void)
{
	return name;
}

int Support::getRank(void)
{
        return rank;
}

int Support::getXp(void)
{
        return xp;
}

int Support::getStamina(void)
{
        return stamina;
}

int Support::getHealth(void)
{
        return health;
}

void Support::getDamage(int damage)
{
  health -= damage;
  if (health < 0)
    health = 0;
}

bool Support::isAlive(void)
{
        if (health == 0)
                return false;
       else
                return true;
}

Weapon * Support::choosePrimary(void)
{
        string infantryPrimary[4] = {"M27","RPK","M249","PKP Pecheneg"};
        cout << "Please choose a primary weapon (1-4) for " << name << endl;
        cout << infantryPrimary[0] << endl;
        cout << infantryPrimary[1] << endl;
        cout << infantryPrimary[2] << endl;
        cout << infantryPrimary[3] << endl;

        int choice;
        cin >> choice;
        primaryWeapon = infantryPrimary[choice - 1];
	cout << endl;
        Weapon * wep = weaponReturn(primaryWeapon);
        return wep;
}

Weapon * Support::chooseSecondary(void)
{
        string infantrySecondary[4] = {"MP433", ".44 Magnum", "M1911", "Glock 17"};
        cout << "Please choose a secondary weapon (1-4) for " << name << endl;
        cout << infantrySecondary[0] << endl;
        cout << infantrySecondary[1] << endl;
        cout << infantrySecondary[2] << endl;
        cout << infantrySecondary[3] << endl;

        int choice;
        cin >> choice;

        secondaryWeapon = infantrySecondary[choice - 1];
	cout << endl;
        Weapon * wep = weaponReturn(secondaryWeapon);
        return wep;
}
/*
Weapon * weaponReturn(string weaponName)
{
 
  string weaponList[20] = {"MP433",".44 Magnum","M1911","Glock 17","AK-47","M16","FN2000","AEK-971","AKS-74u","M4","SCAR-H","G36C","M27","RPK","M249","PKP Pecheneg","MK11","SVD","SV98","M82"};
  int find;
  int i;
  
  for (i = 0; i < 20; i++)
  {
    if (weaponName.compare(weaponList[i]) == 0)
    {
      find = i;
      break;
    }
  }

  switch (find)
  {
  case 0:
    return new MP433();
  case 1:
    return new Magnum();
  case 2: 
    return new M1911();
  case 3:
    return new Glock();
  case 4:
    return new AK47();
  case 5:
    return new M16();
  case 6:
    return new FN2000();
  case 7:
    return new AEK971();
  case 8: 
    return new AKS();
  case 9:
    return new M4();
  case 10:
    return new SCAR();
  case 11:
    return new G36C();
  case 12:
    return new M27();
  case 13:
    return new RPK();
  case 14:
    return new M249();
  case 15:
    return new PKP();
  case 16:
    return new MK11();
  case 17:
    return new SVD();
  case 18:
    return new SV98();
  case 19:
    return new M82();
  }
  return new AK47();
}

*/

Weapon * Support::getPrimary(void)
{
	return primary;
}

Weapon *Support::getSecondary(void)
{
	return secondary;
}


void Support::lockPrimary(Weapon * primary)
{
  this -> primary = primary;
}

void Support::lockSecondary(Weapon * secondary)
{
  this -> secondary = secondary;
}

void Support::outputPrimary(void)
{
  cout << "PRIMARY NAME: " << primary -> getName() << endl;
  cout << "PRIMARY DAMAGE: " << primary -> getDamage() << endl;
  cout << "PRIMARY ACCURACY: " << primary -> getAccuracy() << endl;
  cout << "PRIMARY MAGAZINE: " << primary -> getMagazine() << endl;
  cout << endl;
}

void Support::outputSecondary(void)
{
  cout << "SECONDARY NAME: " << secondary -> getName() << endl;
  cout << "SECONDARY DAMAGE: " << secondary -> getDamage() << endl;
  cout << "SECONDARY ACCURACY: " << secondary -> getAccuracy() << endl;
  cout << "SECONDARY MAGAZINE: " << secondary -> getMagazine() << endl;
  cout << endl;

}


void Support::outputSoldiers(void)
{

    cout << "NAME: " << name << endl;
    cout << "TYPE: SUPPORT" << endl;
    cout << "TEAM: " << teamName << endl;
    cout << "RANK: " << rank << endl;
    cout << "XP: " << xp << endl;
    cout << "HEALTH: " << health << endl;
    cout << "STAMINA: " << stamina << endl;
    cout << endl;
}
