#include "Team.h"
#include "Soldier.h"
#include <string>
#include <iostream>
#include <vector>

using std::string;
using std::cout;
using std::endl;

//int live_count = 0;

Russian::Russian()
{
  this -> maxNumber = 12;
}

Russian::Russian(std::vector<Soldier *> soldierList,int maxNumber)
{
  if (maxNumber == 0)
    maxNumber = 1;
  else if (maxNumber > 12)
    maxNumber = 12;

  this -> maxNumber = maxNumber;
  this -> soldierList = soldierList;
}

Russian::~Russian()
{
}


bool Russian::isOperational(void)
{
  if (liveSoldiers.size() > 0)
    return true;
  return false;
}

std::vector<Soldier *> Russian::getSoldiers(void)
{
  return liveSoldiers;
}

std::vector<Soldier *> Russian::aliveMembers(void)
{
        int live_count = 0;
        int i;
        int num = soldierList.size();
        for(i = 0; i  < num; i++)
        {
                if(soldierList[i] -> getHealth() != 0)
                {
                        if(soldierList[i] -> getType().compare("Infantry") == 0)
                          liveSoldiers.push_back(new Infantry);
                        
                        else if (soldierList[i] -> getType().compare("Engineer") == 0)
                          liveSoldiers.push_back(new Engineer);
                        
                        else if(soldierList[i] -> getType().compare("Support") == 0)
                        liveSoldiers.push_back(new Support);
                        
                        else if(soldierList[i] -> getType().compare("Sniper") == 0)
                          liveSoldiers.push_back(new Sniper);
                        
                        liveSoldiers[live_count] = soldierList[i];
                                 
                        live_count ++;
                }
        }
        return liveSoldiers;

}


std::vector<Soldier *> Russian::deadMembers(void)
{
       int live_count = 0;
        int i;
        int num = soldierList.size();
        for(i = 0; i  < num; i++)
        {
                if(soldierList[i] -> getHealth() == 0)
                {
                        if(soldierList[i] -> getType().compare("Infantry") == 0)
                          deadSoldiers.push_back(new Infantry);
                        
                        else if (soldierList[i] -> getType().compare("Engineer") == 0)
                          deadSoldiers.push_back(new Engineer);
                        
                        else if(soldierList[i] -> getType().compare("Support") == 0)
                        deadSoldiers.push_back(new Support);
                        
                        else if(soldierList[i] -> getType().compare("Sniper") == 0)
                          deadSoldiers.push_back(new Sniper);
                        
                        deadSoldiers[live_count] = soldierList[i];
                                 
                        live_count ++;
                }
        }
        return deadSoldiers;

}

