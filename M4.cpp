#include "EngineerPrimary.h"
#include "Soldier.h"
#include <string>

using std::string;

M4::M4()
{
  name = "M4";
}

string M4::getName(void)
{
        return name;
}

void M4::inputDamage(void)
{
        damage = 25;
}

int M4::getDamage(void)
{
        return damage;
}

void M4::inputAccuracy(void)
{
        accuracy = 35;
}

int M4::getAccuracy(void)
{
        return accuracy;
}

void M4::inputMagazine(void)
{
        magSize = 30;
}

int M4::getMagazine(void)
{
        return magSize;
}

bool M4::isEmpty(void)
{
        if (magSize == 0)
                return true;
        return false;
}


