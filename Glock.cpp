#include "Secondary.h"
#include "Soldier.h"
#include <string>

using std::string;

Glock::Glock()
{
  name = "Glock 17";
}

string Glock::getName(void)
{
        return name;
}

void Glock::inputDamage(void)
{
        damage = 20;
}

int Glock::getDamage(void)
{
        return damage;
}

void Glock::inputAccuracy(void)
{
        accuracy = 23;
}

int Glock::getAccuracy(void)
{
        return accuracy;
}

void Glock::inputMagazine(void)
{
        magSize = 12;
}

int Glock::getMagazine(void)
{
        return magSize;
}

bool Glock::isEmpty(void)
{
        if (magSize == 0)
                return true;
        return false;
}
