#include "SniperPrimary.h"
#include "Soldier.h"
#include <string>

using std::string;

SV98::SV98()
{
  name = "SV98";
}

string SV98::getName(void)
{
        return name;
}

void SV98::inputDamage(void)
{
        damage = 75;
}

int SV98::getDamage(void)
{
        return damage;
}

void SV98::inputAccuracy(void)
{
        accuracy = 50;
}

int SV98::getAccuracy(void)
{
        return accuracy;
}

void SV98::inputMagazine(void)
{
        magSize = 10;
}

int SV98::getMagazine(void)
{
	return magSize;
}


bool SV98::isEmpty(void)
{
        if (magSize == 0)
                return true;
        return false;
}

