#include "rightTools.h"

#ifndef ENGINEERTOOLS_H
#define ENGINEERTOOLS_H

class RPG
{
  RPG();
  
  string getName(void);
  void doesDam(void);
  int getDamage(void);
}

class SMAW
{
  SMAW();
  
  string getName(void);
  void doesDam(void);
  int getDamage(void);
}

class Javelin
{
  Javelin();

  string getName(void);
  void doesDam(void);
  int getDamage(void);
}

class IGLA
{
  IGLA();

  string getName(void);
  void doesDam(void);
  int getDamage(void);
}

class Stinger
{
  Stinger();
  
  string getName(void);
  void doesDam(void);
  int getDamage(void);
}

class Repair
{
  Repair();
  
  string getName(void);
  void doesDam(void);
  int getDamage(void);
}

#endif
