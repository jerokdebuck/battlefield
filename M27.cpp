#include "SupportPrimary.h"
#include "Soldier.h"
#include <string>

using std::string;

M27::M27()
{
  name = "M27";
}

string M27::getName(void)
{
        return name;
}

void M27::inputDamage(void)
{
        damage = 50;
}

int M27::getDamage(void)
{
        return damage;
}

void M27::inputAccuracy(void)
{
        accuracy = 20;
}

int M27::getAccuracy(void)
{
        return accuracy;
}

void M27::inputMagazine(void)
{
        magSize = 30;
}

int M27::getMagazine(void)
{
	return magSize;
}

bool M27::isEmpty(void)
{
        if (magSize == 0)
                return true;
        return false;
}

