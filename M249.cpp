#include "SupportPrimary.h"
#include "Soldier.h"
#include <string>

using std::string;

M249::M249()
{
  name = "M249";
}

string M249::getName(void)
{
        return name;
}

void M249::inputDamage(void)
{
        damage = 30;
}

int M249::getDamage(void)
{
        return damage;
}

void M249::inputAccuracy(void)
{
        accuracy = 25;
}

int M249::getAccuracy(void)
{
        return accuracy;
}

void M249::inputMagazine(void)
{
        magSize = 100;
}

int M249::getMagazine(void)
{
	return magSize;
}
bool M249::isEmpty(void)
{
        if (magSize == 0)
                return true;
        return false;
}

