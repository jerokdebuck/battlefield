#include "SniperPrimary.h"
#include "Soldier.h"
#include <string>

using std::string;

M82::M82()
{
  name = "M82";
}

string M82::getName(void)
{
        return name;
}

void M82::inputDamage(void)
{
        damage = 75;
}

int M82::getDamage(void)
{
        return damage;
}

void M82::inputAccuracy(void)
{
        accuracy = 55;
}

int M82::getAccuracy(void)
{
        return accuracy;
}

void M82::inputMagazine(void)
{
        magSize = 6;
}

int M82::getMagazine(void)
{
	return magSize;
}

bool M82::isEmpty(void)
{
        if (magSize == 0)
                return true;
        return false;
}


