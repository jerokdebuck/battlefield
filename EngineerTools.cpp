#include "EngineerTools.h"
#include <string>

using std::string;

RPG::RPG()
{
  this -> name = "RPG";
}

string RPG::getName(void)
{
  return name;
}

void RPG::doesDam(void)
{
  this -> doesDamage = true;
  this -> damage = 80;
}

int RPG::getDamage(void)
{
  return damage;
}


SMAW::SMAW()
{
  this -> name = "SMAW";
}

string SMAW::getName(void)
{
  return name;
}

void SMAW::doesDam(void)
{
  this -> doesDamage= true;
  this -> damage = 80;
}

int SMAW::getDamage(void)
{
  return damage;
}


Javelin::Javelin()
{
  this -> name = "Javelin";
}

string Javelin::getName(void)
{
  return name;
}

void Javelin::doesDam(void)
{
  this -> doesDamage = true;
  this -> damage = 90;
}

int Javelin::getDamage(void)
{
  return damage;
}

IGLA::IGLA()
{
  this -> name = "IGLA";
}

string IGLA::getName(void)
{
  return name;
}

void IGLA::doesDam(void)
{
  this -> doesDamage = true;
  this -> damage = 65;
}

int IGLA::getDamage(void)
{
  return damage;
}

Stinger::Stinger()
{
  this -> name = "Stinger";
}

string Stinger::getName(void)
{
  return name;
}

void Stinger::doesDam(void)
{
  this -> doesDamage = true;
  this -> damage = 65;
}

int Stinger::getDamage(void)
{
  return damage;
}


Repair::Repair()
{
  this -> name = "Repair Tool";
}

string Repair::getName(void)
{
  return name;
}

void Repair::doesDam(void)
{
  this -> doesDamage = false;
  this -> damage = 0;
}

int Repair::getDamage(void)
{
  return damage;
}
