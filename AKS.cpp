#include "EngineerPrimary.h"
#include "Soldier.h"
#include <string>

using std::string;

AKS::AKS()
{
  name = "AKS-74u";
}

string AKS::getName(void)
{
        return name;
}

void AKS::inputDamage(void)
{
        damage = 30;
}

int AKS::getDamage(void)
{
        return damage;
}

void AKS::inputAccuracy(void)
{
        accuracy = 22;
}

int AKS::getAccuracy(void)
{
        return accuracy;
}

void AKS::inputMagazine(void)
{
        magSize = 30;
}

int AKS::getMagazine(void)
{
        return magSize;
}

bool AKS::isEmpty(void)
{
        if (magSize == 0)
                return true;
        return false;
}
           
