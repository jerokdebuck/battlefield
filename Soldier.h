#include <string>
#include <vector>
class Weapon;


#ifndef SOLDIER_H
#define SOLDIER_H

using std::string;

class Soldier
{
	protected:
		int health;
		int stamina;
		int rank;
		int xp;
		string name;
		string teamName;
    Weapon * primary;
    Weapon * secondary;
	public:
		virtual ~Soldier();
		void inputHealth(int);
		void inputStamina(int);
		void inputRank(int);
    void inputXp(int);
		void inputName(string);
    void inputSide(string);

    const Soldier & operator=(const Soldier &);

		virtual string getType() = 0;
		virtual string getName() = 0;
		virtual string getTeamName() = 0;
		virtual int getRank() = 0;
		virtual int getXp() = 0;
		virtual int getStamina() = 0;
		virtual int getHealth() = 0;
		virtual bool isAlive() = 0;
		virtual Weapon * choosePrimary() = 0;
		virtual Weapon * chooseSecondary() = 0;
		virtual Weapon * getPrimary() = 0;
		virtual Weapon * getSecondary() = 0;
    virtual void lockPrimary(Weapon *) = 0;
    virtual void lockSecondary(Weapon *) = 0;
    virtual void outputPrimary() = 0;
    virtual void outputSecondary() = 0;
    virtual void getDamage(int) = 0;
    virtual void outputSoldiers() = 0;
		
};

class Infantry: public Soldier
{
	private:
		string primaryWeapon;
		string secondaryWeapon;
	public:
		Infantry();
		Infantry(int,int,int,int,string,string);
    ~Infantry();

    string getType();
		string getTeamName();
		string getName();
		int getRank();
		int getXp();
		int getStamina();
		int getHealth();
		bool isAlive();
		Weapon * choosePrimary();
		Weapon * chooseSecondary();
		Weapon * getPrimary();
		Weapon * getSecondary();
    void  lockPrimary(Weapon *);
    void lockSecondary(Weapon *);
    void outputPrimary();
    void outputSecondary();
		void getDamage(int);
    void outputSoldiers();

		const Infantry & operator=(const Infantry &);

};

class Engineer: public Soldier
{
	private:
		string primaryWeapon;
		string secondaryWeapon;
	public:
		Engineer();
	  Engineer(int,int,int,int,string,string);
    ~Engineer();

		string getType();
		string getTeamName();
		string getName();
                int getRank();
                int getXp();
                int getStamina();
                int getHealth();
                bool isAlive();
                Weapon * choosePrimary();
                Weapon * chooseSecondary();
		Weapon * getPrimary();
		Weapon * getSecondary();
                void lockPrimary(Weapon *);
                void lockSecondary(Weapon *);
                void outputPrimary();
                void outputSecondary();
                void getDamage(int);
                
    void outputSoldiers();
		const Engineer & operator=(const Engineer &);
	
};

class Support: public Soldier
{
	private:
		string primaryWeapon;
		string secondaryWeapon;
     	public:
		Support();
		Support(int,int,int,int,string,string);
    ~Support();
    string getType();
		string getTeamName();
		string getName();
    int getRank();
                int getXp();
                int getStamina();
                int getHealth();
                bool isAlive();
                Weapon * choosePrimary();
                Weapon * chooseSecondary();
		Weapon * getPrimary();
		Weapon * getSecondary();
                void lockPrimary(Weapon *);
                void lockSecondary(Weapon *);
                void outputPrimary();
                void outputSecondary();

                void outputSoldiers();
                void getDamage(int);
		const Support & operator=(const Support &);
};

class Sniper: public Soldier
{ 	
	 private:
		string primaryWeapon;
		string secondaryWeapon;
   	 public:
		Sniper();
		Sniper(int,int,int,int,string,string);
    ~Sniper();

                string getType();
		string getTeamName();
		string getName();
                int getRank();
                int getXp();
                int getStamina();
                int getHealth();
                bool isAlive();
                Weapon * choosePrimary();
                Weapon * chooseSecondary();
		Weapon * getPrimary();
		Weapon * getSecondary();

                void lockPrimary(Weapon *);
                void lockSecondary(Weapon *);
                void outputPrimary();
                void outputSecondary();

            void outputSoldiers();
                void getDamage(int);
		const Sniper & operator=(const Sniper &);


};


#endif //SOLDIER_H
