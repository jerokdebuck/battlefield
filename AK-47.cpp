#include "InfantryPrimary.h"
#include "Soldier.h"
#include <string>

using std::string;

AK47::AK47()
{
  name = "AK-47";
}

const AK47 & AK47::operator=(const AK47 & fromAK)
{
  if (this != & fromAK)
  {
    name = fromAK.name;
    damage = fromAK.damage;
    accuracy = fromAK.accuracy;
    magSize = fromAK.magSize;
  }
  return *this;
}

string AK47::getName(void)
{
	return name;
}

void AK47::inputDamage(void)
{
	damage = 35;
}

int AK47::getDamage(void)
{
	return damage;
}

void AK47::inputAccuracy(void)
{
	accuracy = 15;
}

int AK47::getAccuracy(void)
{
	return accuracy;
}

void AK47::inputMagazine(void)
{
	magSize = 30;
}

int AK47::getMagazine(void)
{
	return magSize;
}

bool AK47::isEmpty(void)
{
	if (magSize == 0)
		return true;
	return false;
}
