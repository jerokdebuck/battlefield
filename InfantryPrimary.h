#include <string>
#include "Soldier.h"
#include "Weapon.h"

#ifndef INFANTRYPRIMARY_H
#define INFANTRYPRIMARY_H

using std::string; 

/* 
class infantryWeapon: public Weapon
{
	protected:
		string name;
		int damage;
		int accuracy;
		int magSize;
	public:
		void insertName(string wepName)
		{ name = wepName; }
		virtual string getName(void) = 0;
		virtual void inputDamage(void) = 0;
		virtual int getDamage(void) = 0;
		virtual void inputAccuracy(void) = 0;
		virtual int getAccuracy(void) = 0;
		virtual void inputMagazine(void) = 0;
		virtual int getMagazine(void) = 0;
		virtual bool isEmpty(void) = 0;
};
*/
 
class AK47: public Weapon
{
	public:
    AK47();

    const AK47 & operator=(const AK47 &);
		string getName(void);
		void inputDamage(void);
		int getDamage(void);
		void inputAccuracy(void);
		int getAccuracy(void);
		void inputMagazine(void);
		int getMagazine(void);
		bool isEmpty(void);
};

class M16: public Weapon
{
	public:
        M16();

    		string getName(void);
        	void inputDamage(void);
        	int getDamage(void);
        	void inputAccuracy(void);
        	int getAccuracy(void);
        	void inputMagazine(void);
        	int getMagazine(void);
        	bool isEmpty(void);
};

class FN2000: public Weapon
{
	public:
          FN2000();
		string getName(void);
        	void inputDamage(void);
        	int getDamage(void);
        	void inputAccuracy(void);
        	int getAccuracy(void);
        	void inputMagazine(void);
       	 	int getMagazine(void);
        	bool isEmpty(void);
};

class AEK971: public Weapon
{
	public:
          AEK971();
		string getName(void);
		void inputDamage(void);
        	int getDamage(void);
        	void inputAccuracy(void);
        	int getAccuracy(void);
        	void inputMagazine(void);
        	int getMagazine(void);
        	bool isEmpty(void);
};


#endif
