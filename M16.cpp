#include "InfantryPrimary.h"
#include <string>

using std::string;

M16::M16()
{
  name = "M16";
}

string M16::getName(void)
{
	return name;
}

void M16::inputDamage(void)
{
        damage = 35;
}

int M16::getDamage(void)
{
        return damage;
}

void M16::inputAccuracy(void)
{
        accuracy = 22;
}

int M16::getAccuracy(void)
{
        return accuracy;
}

void M16::inputMagazine(void)
{
        magSize = 30;
}

int M16::getMagazine(void)
{
        return magSize;
}

bool M16::isEmpty(void)
{
	if (magSize == 0)
		return true;
	return false;
}
