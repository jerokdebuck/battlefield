#include "SniperPrimary.h"
#include "Soldier.h"
#include <string>

using std::string;

SVD::SVD()
{
  name = "SVD";
}

string SVD::getName(void)
{
        return name;
}

void SVD::inputDamage(void)
{
        damage = 45;
}

int SVD::getDamage(void)
{
        return damage;
}

void SVD::inputAccuracy(void)
{
        accuracy = 40;
}

int SVD::getAccuracy(void)
{
        return accuracy;
}

void SVD::inputMagazine(void)
{
        magSize = 10;
}

int SVD::getMagazine(void)
{
	return magSize;
}

bool SVD::isEmpty(void)
{
        if (magSize == 0)
                return true;
        return false;
}

