#include "SupportPrimary.h"
#include "Soldier.h"
#include <string>

using std::string;

PKP::PKP()
{
  name = "PKP Pecheneg";
}

string PKP::getName(void)
{
        return name;
}

void PKP::inputDamage(void)
{
        damage = 35;
}

int PKP::getDamage(void)
{
        return damage;
}

void PKP::inputAccuracy(void)
{
        accuracy = 20;
}

int PKP::getAccuracy(void)
{
        return accuracy;
}

void PKP::inputMagazine(void)
{
        magSize = 100;
}

int PKP::getMagazine(void)
{
	return magSize;
}

bool PKP::isEmpty(void)
{
        if (magSize == 0)
                return true;
        return false;
}

