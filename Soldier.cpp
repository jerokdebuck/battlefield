#include "Soldier.h"
#include "Weapon.h"
#include <cstdlib>

Soldier::~Soldier()
{
  delete primary;
  delete secondary;
}

void Soldier::inputHealth(int health)
{
  if (health < 0)
    health = 0;
  else if (health > 100)
    health = 100;

  this -> health = health;
}

void Soldier::inputStamina(int stamina)
{
  if (stamina < 0)
    stamina = 0;
  
  else if (stamina > 100)
    stamina = 100;

  this -> stamina = stamina;
}

void Soldier::inputRank(int rank)
{
  if (rank <= 0) 
    rank = 1;

  else if (rank > 45)
    rank = 45;

  this -> rank = rank;
}

void Soldier::inputXp(int xp)
{
  this -> xp = xp;
}

void Soldier::inputName(string name)
{
  this -> name = name;
}

void Soldier::inputSide(string teamName)
{
  if((teamName.compare("American") != 0) && teamName.compare("Russian") != 0)
  {
    int x = rand() % 2;
    if (x == 0)
      teamName = "American";
    else if (x == 1)
      teamName = "Russian";
  }

  this -> teamName = teamName;
}

const Soldier & Soldier::operator=(const Soldier & fromSoldier)
{
  if (this != & fromSoldier)
  {
    health = fromSoldier.health;
    stamina = fromSoldier.stamina;
    rank = fromSoldier.rank;
    xp = fromSoldier.xp;
    name = fromSoldier.name;
    teamName = fromSoldier.teamName;
    primary = fromSoldier.primary;
    secondary = fromSoldier.secondary;
  }
  return *this;
}

